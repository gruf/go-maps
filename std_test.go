package maps_test

import (
	"testing"
)

func BenchmarkStdMapRead(b *testing.B) {
	stringMap := make(map[string]any, 0)
	intMap := make(map[int64]any, 0)
	floatMap := make(map[float64]any, 0)

	for _, entry := range stringKeyBenchmarks {
		stringMap[entry.Key] = entry.Value
	}

	for _, entry := range intKeyBenchmarks {
		intMap[entry.Key] = entry.Value
	}

	for _, entry := range floatKeyBenchmarks {
		floatMap[entry.Key] = entry.Value
	}

	// Start test timer
	b.ResetTimer()
	b.StartTimer()

	for i := 0; i < b.N; i++ {
		var ok bool
		var val any

		// Read via each string key from map
		for _, entry := range stringKeyBenchmarks {
			val, ok = stringMap[entry.Key]
		}

		// Read via each int64 key from map
		for _, entry := range intKeyBenchmarks {
			val, ok = intMap[entry.Key]
		}

		// Read via each float64 key from map
		for _, entry := range floatKeyBenchmarks {
			val, ok = floatMap[entry.Key]
		}

		_ = ok
		_ = val
	}
}

func BenchmarkStdMapWrite(b *testing.B) {
	stringMap := make(map[string]any, 0)
	intMap := make(map[int64]any, 0)
	floatMap := make(map[float64]any, 0)

	// Start test timer
	b.ResetTimer()
	b.StartTimer()

	for i := 0; i < b.N; i++ {
		// Set each string key in the map
		for _, entry := range stringKeyBenchmarks {
			stringMap[entry.Key] = entry.Value
		}

		// Set each int64 key in the map
		for _, entry := range intKeyBenchmarks {
			intMap[entry.Key] = entry.Value
		}

		// Set each float64 key in the map
		for _, entry := range floatKeyBenchmarks {
			floatMap[entry.Key] = entry.Value
		}

		// Delete each string key in the map
		for _, entry := range stringKeyBenchmarks {
			delete(stringMap, entry.Key)
		}

		// Delete each int64 key in the map
		for _, entry := range intKeyBenchmarks {
			delete(intMap, entry.Key)
		}

		// Delete each float64 key in the map
		for _, entry := range floatKeyBenchmarks {
			delete(floatMap, entry.Key)
		}
	}
}
