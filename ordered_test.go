package maps_test

import (
	"testing"

	"codeberg.org/gruf/go-maps"
)

func TestOrderedMapRW(t *testing.T) {
	m := maps.NewOrdered[string, string](0)

	// Add keys to the map
	for _, test := range tests {
		if !m.Add(test.Key, test.Value) {
			t.Fatal("failed adding key to map")
		}
	}

	// Ensure is expected length
	if m.Len() != len(tests) {
		t.Fatal("failed confirming correct map length")
	}

	// Check map contains keys
	for _, test := range tests {
		if !m.Has(test.Key) {
			t.Fatal("failed checking if map contains key")
		}
	}

	// Ensure adding a second time fails
	for _, test := range tests {
		if m.Add(test.Key, test.Value) {
			t.Fatal("failed re-adding existing key to map")
		}
	}

	// Overwrite all the key values
	for _, test := range tests {
		m.Set(test.Key, test.Value+" ")
	}

	// Ensure key values all updated
	for _, test := range tests {
		v, _ := m.Get(test.Key)
		if v != test.Value+" " {
			t.Fatal("failed confirming set key has correct value")
		}
	}

	// Delete all keys from map
	for _, test := range tests {
		if !m.Delete(test.Key) || m.Has(test.Key) {
			t.Fatal("failed deleting key from map")
		}
	}

	// Ensure is expected length
	if m.Len() != 0 {
		t.Fatal("failed confirming correct map length")
	}
}

func TestOrderedMapRange(t *testing.T) {
	m := maps.NewOrdered[string, string](0)

	// Add keys to the map
	for _, test := range tests {
		if !m.Add(test.Key, test.Value) {
			t.Fatal("failed adding key to map")
		}
	}

	added := map[string]string{}

	// Check keys are in the correct order in the map
	m.Range(0, m.Len(), func(i int, key, value string) {
		for idx, test := range tests {
			if test.Key == key {
				if idx != i {
					t.Fatal("key at unexpected index in map")
				}
				added[key] = value
				return
			}
		}
		t.Fatal("unknown key during range")
	})

	// Check all keys were ranged over
	if len(added) != len(tests) {
		t.Fatal("did not range over all keys in map")
	}

	added = map[string]string{}

	// Check in reverse that keys are in correct order
	m.Range(m.Len()-1, -m.Len(), func(i int, key, value string) {
		for idx, test := range tests {
			if test.Key == key {
				if idx != i {
					t.Fatalf("key at unexpected index in map: %d %+v", i, test)
				}
				added[key] = value
				return
			}
		}
		t.Fatal("unknown key during range")
	})

	// Check all keys were ranged over
	if len(added) != len(tests) {
		t.Fatal("did not range over all keys in map")
	}
}

func TestOrderedMapIndex(t *testing.T) {
	m := maps.NewOrdered[string, string](0)

	// Add keys to the map
	for _, test := range tests {
		if !m.Add(test.Key, test.Value) {
			t.Fatal("failed adding key to map")
		}
	}

	// Ensure index finds expected keys
	for i, test := range tests {
		key, value, ok := m.Index(i)
		if !ok {
			t.Fatal("failed finding key at index in map")
		} else if key != test.Key || value != test.Value {
			t.Fatal("failed confirming key at index contains expected key-value")
		}
	}
}

func TestOrderedMapTruncate(t *testing.T) {
	m := maps.NewOrdered[string, string](0)

	// Add keys to the map
	for _, test := range tests {
		if !m.Add(test.Key, test.Value) {
			t.Fatal("failed adding key to map")
		}
	}

	dropped := map[string]string{}

	for i := 0; m.Len() > 0; i++ {
		if i > m.Len() {
			i = m.Len()
		}

		// Drop next 'i' many keys from map
		m.Truncate(i, func(key, value string) {
			dropped[key] = value
		})
	}

	// Ensure is expected length
	if m.Len() != 0 {
		t.Fatal("failed confirming correct map length: ", m)
	}

	// Ensure expected keys dropped
	if len(dropped) != len(tests) {
		t.Fatal("did not drop all keys from map")
	}
}

func BenchmarkOrderedMapRead(b *testing.B) {
	stringMap := maps.NewOrdered[string, any](0)
	intMap := maps.NewOrdered[int64, any](0)
	floatMap := maps.NewOrdered[float64, any](0)

	for _, entry := range stringKeyBenchmarks {
		stringMap.Add(entry.Key, entry.Value)
	}

	for _, entry := range intKeyBenchmarks {
		intMap.Add(entry.Key, entry.Value)
	}

	for _, entry := range floatKeyBenchmarks {
		floatMap.Add(entry.Key, entry.Value)
	}

	// Start test timer
	b.ResetTimer()
	b.StartTimer()

	for i := 0; i < b.N; i++ {
		var ok bool
		var val any

		// Read via each string key from map
		for _, entry := range stringKeyBenchmarks {
			val, ok = stringMap.Get(entry.Key)
		}

		// Read via each int64 key from map
		for _, entry := range intKeyBenchmarks {
			val, ok = intMap.Get(entry.Key)
		}

		// Read via each float64 key from map
		for _, entry := range floatKeyBenchmarks {
			val, ok = floatMap.Get(entry.Key)
		}

		_ = ok
		_ = val
	}
}

func BenchmarkOrderedMapWrite(b *testing.B) {
	stringMap := maps.NewOrdered[string, any](0)
	intMap := maps.NewOrdered[int64, any](0)
	floatMap := maps.NewOrdered[float64, any](0)

	// Start test timer
	b.ResetTimer()
	b.StartTimer()

	for i := 0; i < b.N; i++ {
		// Set each string key in the map
		for _, entry := range stringKeyBenchmarks {
			stringMap.Set(entry.Key, entry.Value)
		}

		// Set each int64 key in the map
		for _, entry := range intKeyBenchmarks {
			intMap.Set(entry.Key, entry.Value)
		}

		// Set each float64 key in the map
		for _, entry := range floatKeyBenchmarks {
			floatMap.Set(entry.Key, entry.Value)
		}

		// Delete each string key in the map
		for _, entry := range stringKeyBenchmarks {
			stringMap.Delete(entry.Key)
		}

		// Delete each int64 key in the map
		for _, entry := range intKeyBenchmarks {
			intMap.Delete(entry.Key)
		}

		// Delete each float64 key in the map
		for _, entry := range floatKeyBenchmarks {
			floatMap.Delete(entry.Key)
		}
	}
}
