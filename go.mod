module codeberg.org/gruf/go-maps

go 1.20

require codeberg.org/gruf/go-kv v1.6.5

require codeberg.org/gruf/go-byteutil v1.2.0
