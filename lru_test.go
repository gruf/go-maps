package maps_test

import (
	"testing"

	"codeberg.org/gruf/go-maps"
)

func TestLRUMapRW(t *testing.T) {
	m := maps.NewLRU[string, string](0, len(tests)+1)

	// Add keys to the map
	for _, test := range tests {
		if !m.Add(test.Key, test.Value) {
			t.Fatal("failed adding key to map")
		}
	}

	// Ensure is expected length
	if m.Len() != len(tests) {
		t.Fatal("failed confirming correct map length")
	}

	// Check map contains keys
	for _, test := range tests {
		if !m.Has(test.Key) {
			t.Fatal("failed checking if map contains key")
		}
	}

	// Ensure adding a second time fails
	for _, test := range tests {
		if m.Add(test.Key, test.Value) {
			t.Fatal("failed re-adding existing key to map")
		}
	}

	// Overwrite all the key values
	for _, test := range tests {
		m.Set(test.Key, test.Value+" ")
	}

	// Ensure key values all updated
	for _, test := range tests {
		v, _ := m.Get(test.Key)
		if v != test.Value+" " {
			t.Fatal("failed confirming set key has correct value")
		}
	}

	// Delete all keys from map
	for _, test := range tests {
		if !m.Delete(test.Key) || m.Has(test.Key) {
			t.Fatal("failed deleting key from map")
		}
	}

	// Ensure is expected length
	if m.Len() != 0 {
		t.Fatal("failed confirming correct map length")
	}
}

func TestLRUMapRange(t *testing.T) {
	m := maps.NewLRU[string, string](0, 100)

	// Add keys to the map
	for _, test := range tests {
		if !m.Add(test.Key, test.Value) {
			t.Fatal("failed adding key to map")
		}
	}

	// Check keys are in reverse order
	m.Range(0, m.Len(), func(i int, key, value string) {
		idx := len(tests) - i - 1
		if tests[idx].Key != key {
			t.Fatal("key not at expected index")
		}
	})

	// Check that reverse range returns them in reverse order
	m.Range(m.Len()-1, -m.Len(), func(i int, key, value string) {
		idx := len(tests) - i - 1
		if tests[idx].Key != key {
			t.Fatal("key not at expected index")
		}
	})
}

func TestLRUMapTruncate(t *testing.T) {
	m := maps.NewLRU[string, string](0, 100)

	// Add keys to the map
	for _, test := range tests {
		if !m.Add(test.Key, test.Value) {
			t.Fatal("failed adding key to map")
		}
	}

	dropped := map[string]string{}

	for i := 0; m.Len() > 0; i++ {
		if i > m.Len() {
			i = m.Len()
		}

		// Drop next 'i' many keys from map
		m.Truncate(i, func(key, value string) {
			dropped[key] = value
		})
	}

	// Ensure is expected length
	if m.Len() != 0 {
		t.Fatal("failed confirming correct map length: ", m)
	}

	// Ensure expected keys dropped
	if len(dropped) != len(tests) {
		t.Fatal("did not drop all keys from map")
	}

	// Check that keys were succesfully dropped
	for _, test := range tests {
		_, ok := m.Get(test.Key)
		if ok {
			t.Error("did not drop key from map")
		}
	}
}

func TestLRUMapEvict(t *testing.T) {
	m := maps.NewLRU[string, string](0, len(tests)-5)

	// Add first however many keys to map
	for i := 0; i < m.Cap(); i++ {
		if !m.Add(tests[i].Key, tests[i].Value) {
			t.Fatal("failed adding key to map")
		}
	}

	var evictIdx int

	// Add remaining test entries, check evictions
	for i := m.Cap() - 1; i < len(tests); i++ {
		m.AddWithHook(tests[i].Key, tests[i].Value, func(key, value string) {
			if key != tests[evictIdx].Key || value != tests[evictIdx].Value {
				t.Fatal("unexpected key evicted from map")
			}
			evictIdx++
		})
	}

	// Check that keys were evicted
	if evictIdx == 0 {
		t.Fatal("no entries evicted")
	}

	// Perform a fetch of remaining keys to ensure
	// that underlying evictions reused entries correctly.
	for i := 0; i < m.Len(); i++ {
		_, _ = m.Get(tests[i].Key)
	}
}

func BenchmarkLRUMapRead(b *testing.B) {
	stringMap := maps.NewLRU[string, any](0, 100)
	intMap := maps.NewLRU[int64, any](0, 100)
	floatMap := maps.NewLRU[float64, any](0, 100)

	for _, entry := range stringKeyBenchmarks {
		stringMap.Add(entry.Key, entry.Value)
	}

	for _, entry := range intKeyBenchmarks {
		intMap.Add(entry.Key, entry.Value)
	}

	for _, entry := range floatKeyBenchmarks {
		floatMap.Add(entry.Key, entry.Value)
	}

	// Start test timer
	b.ResetTimer()
	b.StartTimer()

	for i := 0; i < b.N; i++ {
		var ok bool
		var val any

		// Read via each string key from map
		for _, entry := range stringKeyBenchmarks {
			val, ok = stringMap.Get(entry.Key)
		}

		// Read via each int64 key from map
		for _, entry := range intKeyBenchmarks {
			val, ok = intMap.Get(entry.Key)
		}

		// Read via each float64 key from map
		for _, entry := range floatKeyBenchmarks {
			val, ok = floatMap.Get(entry.Key)
		}

		_ = ok
		_ = val
	}
}

func BenchmarkLRUMapWrite(b *testing.B) {
	stringMap := maps.NewLRU[string, any](0, 100)
	intMap := maps.NewLRU[int64, any](0, 100)
	floatMap := maps.NewLRU[float64, any](0, 100)

	// Start test timer
	b.ResetTimer()
	b.StartTimer()

	for i := 0; i < b.N; i++ {
		// Set each string key in the map
		for _, entry := range stringKeyBenchmarks {
			stringMap.Set(entry.Key, entry.Value)
		}

		// Set each int64 key in the map
		for _, entry := range intKeyBenchmarks {
			intMap.Set(entry.Key, entry.Value)
		}

		// Set each float64 key in the map
		for _, entry := range floatKeyBenchmarks {
			floatMap.Set(entry.Key, entry.Value)
		}

		// Delete each string key in the map
		for _, entry := range stringKeyBenchmarks {
			stringMap.Delete(entry.Key)
		}

		// Delete each int64 key in the map
		for _, entry := range intKeyBenchmarks {
			intMap.Delete(entry.Key)
		}

		// Delete each float64 key in the map
		for _, entry := range floatKeyBenchmarks {
			floatMap.Delete(entry.Key)
		}
	}
}
